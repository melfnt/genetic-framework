
from typing import Callable, TypeVar, Iterable, Sequence
import random
import numpy as np

Solution = TypeVar('Solution')

def bitarray_random_solution_generator (N: int, ones_probability=0.5) -> Callable[[],Sequence[int]]:
    return lambda: np.random.choice([0,1], N, p=[1 - ones_probability, ones_probability])

def bitarray_crossover (x: Sequence[int], y: Sequence[int]) -> Sequence[int]:
    n = random.randint (1,len(x)-1)
    return np.hstack ( [x[:n],  y[n:]] )

def bitarray_mutation ( mutation_probability=0.01 ) -> Callable[[Sequence[int]],Sequence[int]]:
    def mutate ( x: Sequence[int], p ):
        for i in range (len(x)):
            if np.random.rand()<p:
                x[i] = 1-x[i]
        return x
    return lambda x: mutate (x, mutation_probability)

def intarray_random_solution_generator (N: int, low: int, high: int) -> Callable[[],Sequence[int]]:
    return lambda: np.random.randint (low, high, N)

def intarray_crossover (x: Sequence[int], y: Sequence[int]) -> Sequence[int]:
    n = random.randint (1,len(x)-1)
    return np.hstack ( [x[:n],  y[n:]] )

def intarray_mutation ( low: int, high: int, mutation_probability=0.01 ) -> Callable[[Sequence[int]],Sequence[int]]:
    def mutate ( x: Sequence[int], p ):
        for i in range (len(x)):
            if np.random.rand()<p:
                x[i] = np.random.randint (low, high)
        return x
    return lambda x: mutate (x, mutation_probability)

def argmax (l: Iterable[float]):
    return max(enumerate(l), key=lambda t: t[1])

# TODO: what if some inp[i] is negative or zero?
def smooth_distribution_probability ( inp: Sequence[float], lamb=0.01 ):
    total = sum(inp) + len(inp) * lamb
    return [ (x+lamb)/total for x in inp ]

def genetic_algorithm ( random_solution_generator: Callable[[], Solution], evaluate_fitness: Callable[[Solution], float], crossover: Callable[[Solution, Solution], Solution], mutation: Callable[[Solution], Solution]=None, population_size=100, offspring_size=None, fitness_threshold=None, max_generations=1000 ):
    '''
        runs the genetic algorithm, yields partial results until a certain fitness treshold is reached or a maximum number of generations are generated.
    '''
    if offspring_size == None:

        offspring_size = population_size
    
    assert offspring_size > 0 and population_size > 0, "offspring size and population_size must be greater than zero"

    # print ("** BEFORE STARTING **")
    population = [random_solution_generator() for _ in range (population_size)]
    fitnesses = [evaluate_fitness (s) for s in population]
    
    # print ("population: {}".format(population))
    # print ("fitnesses: {}".format(fitnesses))
    
    j, best_fitness = argmax(fitnesses)
    # print ("best fitness: {}".format(best_fitness))

    yield population[j]

    generation_count = 0
    while generation_count<max_generations and (fitness_threshold is None or best_fitness<fitness_threshold):

        # print ("** GENERATON {}/{} **".format(generation_count, max_generations))
        # print ("best fitness so far: {}".format(best_fitness))
        # print ("population: {}".format(population))
        # print ("fitnesses: {}".format(fitnesses))

        Xs = random.choices (population, k=offspring_size)
        Ys = random.choices (population, k=offspring_size)
        offsprings = [crossover(x, y) for x,y in zip(Xs, Ys)]
        if mutation is not None:
            offsprings = list(map (mutation, offsprings))
        offspring_fitnesses = [evaluate_fitness (s) for s in offsprings]

        # print ("offsprings: {}".format(offsprings))
        # print ("offsprings_fitnesses: {}".format(offspring_fitnesses))

        probabilities = smooth_distribution_probability (fitnesses + offspring_fitnesses)
        selected_indices = np.random.choice ( range(population_size+offspring_size), population_size, replace=False, p=probabilities)
        population = np.vstack ([population,offsprings])[selected_indices]
        fitnesses = [(fitnesses[i] if i<population_size else offspring_fitnesses[i-population_size]) for i in selected_indices]
    
        j, generation_max_fitness = argmax(fitnesses)
        # print ("best fitness for this generation: {}".format(generation_max_fitness))

        if generation_max_fitness > best_fitness:
            # print ("found new best fitness")
            best_fitness = generation_max_fitness
            yield population[j]

        generation_count += 1
        
        # print ()
        # input()
