
import sys
from typing import List, Tuple, Sequence, Callable

import tqdm

from genetic_framework import genetic_algorithm, bitarray_crossover, bitarray_mutation, bitarray_random_solution_generator

def parse_hc_input_dataset ( filename: str ) -> Tuple[List[int], int]:
    '''
        parses a google hash code 2020 (practice problem) input file.
        returns the list of all the items and max_weight
    '''
    with open(filename) as fin:
        max_weight, _ = [int(x) for x in fin.readline().split()]
        items = [int(x) for x in fin.readline().split()]
    return items, max_weight

def print_hc_dataset ( bitarray: Sequence[int] ):
    '''
        prints a google hash code 2020 (practice problem) solution on stdout.
    '''
    items = [i for (i, select) in enumerate(bitarray) if select]
    print (len(items))
    print (" ".join(str(x) for x in items))

def evaluate_fitness (items: Sequence[int], W: int) -> Callable[[Sequence[int]], float]:
    def fitness (bag: Sequence[int]):
        total = sum ( items[i] for (i, select) in enumerate(bag) if select ) 
        if total > W:
            return 0
        else:
            return total
    
    return lambda x: fitness(x)


if __name__ == "__main__":

    if len(sys.argv) < 2:
        sys.exit("usage: genetic_knapsack.py input.in")
    filename = sys.argv[1]

    items, W = parse_hc_input_dataset (filename)
    N = len(items)
    print ("items: {}, W: {}".format(items, W))
    
    max_generations = 10000

    iterator = genetic_algorithm ( bitarray_random_solution_generator(N,0.01), evaluate_fitness(items, W), bitarray_crossover, bitarray_mutation(), fitness_threshold=W, max_generations=max_generations)
    for bitarray in tqdm.tqdm(iterator, total=max_generations):
        print_hc_dataset (bitarray)

