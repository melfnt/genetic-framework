
import sys
from typing import List, Tuple, Sequence, Dict, Callable

import tqdm

from genetic_framework import genetic_algorithm, intarray_crossover, intarray_mutation, intarray_random_solution_generator

def evaluate_fitness (C: Sequence[int]) -> float:
    clashes = 0
    N = len(C)
    for i in range (N):
        for j in range (i+1,N):
            if C[i]==C[j] or C[j]-C[i]==j-i or C[i]-C[j]==j-i:
                clashes += 1

    return N*(N-1)/2-clashes

def print_chessboard ( C: Sequence[int] ):
    '''
        prints a NxN chessboard with N queens on stdout.
    '''
    N = len(C)
    clashes = N*(N-1)/2-evaluate_fitness (C)
    print ("C = {}".format(C))
    for pos in C:
        print ("."*pos + "Q" + "."*(N-pos-1))
    print ("clashes: {}".format(clashes))


if __name__ == "__main__":

    if len(sys.argv) < 2:
        sys.exit("usage: n_queens.py N")
    N = int(sys.argv[1])
    
    max_generations = 10000

    iterator = genetic_algorithm ( intarray_random_solution_generator(N, 0, N), evaluate_fitness, intarray_crossover, intarray_mutation(0, N), fitness_threshold=N*(N-1)/2, max_generations=max_generations)
    for bitarray in tqdm.tqdm(iterator, total=max_generations):
        print_chessboard (bitarray)

